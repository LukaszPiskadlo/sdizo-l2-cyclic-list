#include <iostream>

using namespace std;

struct Node
{
	int value;
	Node* next;
};

Node* create(int value)
{
	Node* node = new Node;
	node->value = value;
	node->next = node;

	return node;
}

Node* getTail(Node* head)
{
	if (head == nullptr)
	{
		cout << "Lista jest pusta!" << endl;
		return nullptr;
	}

	Node* node = head;
	while (node->next != head)
	{
		node = node->next;
	}

	return node;
}

void add(Node* head, int value)
{
	if (head == nullptr)
	{
		cout << "Lista jest pusta!" << endl;
		return;
	}

	Node* node = new Node;
	node->value = value;
	node->next = head;
	Node* tail = getTail(head);
	tail->next = node;
}

void remove(Node*& head, int value)
{
	if (head == nullptr)
	{
		cout << "Lista jest pusta!" << endl;
		return;
	}

	bool removed = false;
	Node* node = head;
	Node* prev = getTail(head);
	do
	{
		if (node->value == value)
		{
			if (node == head)
				head = node->next;
			
			prev->next = node->next;

			delete node;
			removed = true;
			break;
		}
		prev = node;
		node = prev->next;
	} while (node != head);

	if (!removed)
		cout << "Nie znaleziono elementu do usuniecia!" << endl;
}

void sort(Node* head)
{
	if (head == nullptr)
	{
		cout << "Lista jest pusta!" << endl;
		return;
	}
	else if (head->next == head)
	{
		cout << "Lista jest jednoelementowa!" << endl;
		return;
	}
}

int count(Node* head)
{
	if (head == nullptr)
		return 0;

	int counter = 0;
	Node* node = head;
	do
	{
		counter++;
		node = node->next;
	} while(node != head);

	return counter;
}

void printAll(Node* head)
{
	if (head == nullptr)
	{
		cout << "Lista jest pusta!" << endl;
		return;
	}

	Node* node = head;
	do
	{
		cout << node->value << ' ';
		node = node->next;
	} while (node != head);

	cout << endl;
}

void freeAll(Node* head)
{
	if (head == nullptr)
	{
		cout << "Lista jest pusta!" << endl;
		return;
	}

	Node* node = head;
	Node* temp = nullptr;
	do
	{
		temp = node->next;
		delete node;
		node = temp;
	} while (node != head);
}

int main()
{
	Node* head = create(1);
	add(head, 2);
	add(head, 3);
	add(head, 4);
	add(head, 5);
	printAll(head);
	remove(head, 3);
	remove(head, 8);
	count(head);
	printAll(head);
	sort(head);
	printAll(head);
	freeAll(head);

	system("pause");
	return 0;
}
